import React, { Component } from 'react';
import { View,Dimensions,ActivityIndicator,Modal,StyleSheet,BackHandler,Alert, Image } from 'react-native';
import {WebView} from 'react-native-webview';
import { SafeAreaConsumer } from 'react-native-safe-area-context';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.WEBVIEW_REF = React.createRef();
    this.state = {
        loading:false,
        backButtonEnabled:false,
        webviewref:null,
        screenSize:Dimensions.get('screen')
    };
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener("hardwareBackPress",this.backHandler);
  }
  backHandler = () => {
    if(this.state.backButtonEnabled) {
        this.WEBVIEW_REF.current.goBack();
        return true;
    }
}
  componentWillUnmount() {
    this.backHandler.remove();
  }  
  handleWebViewNavigationStateChange = newNavState => {
      this.setState({loading:newNavState.loading,backButtonEnabled: newNavState.canGoBack});
  }
  render() {
    return (
        <View>
            <Modal animationType="fade" transparent={true} visible={this.state.loading} 
                onRequestClose={() => {}}>
                    <View style={styles.modal}>
                        <Image style={styles.loadingimage} source={require('../../assets/output-onlinepngtools.png')}/>
                    </View>
            </Modal> 
        <SafeAreaConsumer>{insets => 
            <View style={{ paddingTop: insets.top,paddingBottom:insets.bottom }}>
                <View style={styles.main}>
                     <WebView  ref={this.WEBVIEW_REF} 
                     originWhitelist={['*']} 
                     source={{ uri: 'https://eautofix.com/' }} 
                     onNavigationStateChange={this.handleWebViewNavigationStateChange}/>
                </View>
            </View>
        }
        </SafeAreaConsumer>            
        </View>
    );
  }
}

const styles = StyleSheet.create({
    main:{
        height:Dimensions.get('screen').height, 
        width: Dimensions.get('screen').width,
        overflow:'hidden',
    },
    modal:{
        backgroundColor:"#000000",
        opacity:0.8,
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    loadingimage:{
        borderRadius:200,
        width:120,
        height:120
    }
  });